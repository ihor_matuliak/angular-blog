import { Pipe, PipeTransform } from '@angular/core';
import { Post } from 'src/app/admin/shared/interfaces/interfaces';

@Pipe({
  name: 'filterPosts',
})
export class FilterPostsPipe implements PipeTransform {
  transform(posts: Post[], search: string): Post[] {
    if (!search.trim()) {
      return posts;
    }

    return posts.filter((post) =>
      post.title.toLowerCase().includes(search.toLowerCase())
    );
  }
}
